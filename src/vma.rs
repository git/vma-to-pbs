use std::collections::HashSet;
use std::io;
use std::io::Read;
use std::mem::size_of;
use std::str;

use anyhow::{bail, format_err, Context, Error};
use bincode::Options;
use serde::{Deserialize, Serialize};
use serde_big_array::BigArray;

/// Maximum number of clusters in an extent
/// See Image Data Streams in pve-qemu.git/vma_spec.txt
const VMA_CLUSTERS_PER_EXTENT: usize = 59;

/// Number of 4k blocks per cluster
/// See pve-qemu.git/vma_spec.txt
const VMA_BLOCKS_PER_CLUSTER: usize = 16;

/// Maximum number of config files
/// See VMA Header in pve-qemu.git/vma_spec.txt
const VMA_MAX_CONFIGS: usize = 256;

/// Maximum number of block devices
/// See VMA Header in pve-qemu.git/vma_spec.txt
pub const VMA_MAX_DEVICES: usize = 256;

/// VMA magic string
/// See VMA Header in pve-qemu.git/vma_spec.txt
const VMA_HEADER_MAGIC: [u8; 4] = [b'V', b'M', b'A', 0];

/// VMA extent magic string
/// See VMA Extent Header in pve-qemu.git/vma_spec.txt
const VMA_EXTENT_HEADER_MAGIC: [u8; 4] = [b'V', b'M', b'A', b'E'];

/// Size of a block
/// See pve-qemu.git/vma_spec.txt
const BLOCK_SIZE: usize = 4096;

/// Size of the VMA header without the blob buffer appended at the end
/// See VMA Header in pve-qemu.git/vma_spec.txt
const VMA_HEADER_SIZE_NO_BLOB_BUFFER: usize = 12288;

type VmaDeviceId = u8;
type VmaDeviceOffset = u64;
type VmaDeviceSize = u64;

#[repr(C)]
#[derive(Serialize, Deserialize)]
struct VmaDeviceInfoHeader {
    pub device_name_offset: u32,
    reserved: [u8; 4],
    pub device_size: VmaDeviceSize,
    reserved1: [u8; 16],
}

#[repr(C)]
#[derive(Serialize, Deserialize)]
struct VmaHeader {
    pub magic: [u8; 4],
    pub version: u32,
    pub uuid: [u8; 16],
    pub ctime: u64,
    pub md5sum: [u8; 16],
    pub blob_buffer_offset: u32,
    pub blob_buffer_size: u32,
    pub header_size: u32,
    #[serde(with = "BigArray")]
    reserved: [u8; 1984],
    #[serde(with = "BigArray")]
    pub config_names: [u32; VMA_MAX_CONFIGS],
    #[serde(with = "BigArray")]
    pub config_data: [u32; VMA_MAX_CONFIGS],
    reserved1: [u8; 4],
    #[serde(with = "BigArray")]
    pub dev_info: [VmaDeviceInfoHeader; VMA_MAX_DEVICES],
    #[serde(skip_deserializing, skip_serializing)]
    blob_buffer: Vec<u8>,
}

#[repr(C)]
#[derive(Serialize, Deserialize)]
struct VmaBlockInfo {
    pub mask: u16,
    reserved: u8,
    pub dev_id: u8,
    pub cluster_num: u32,
}

#[repr(C)]
#[derive(Serialize, Deserialize)]
struct VmaExtentHeader {
    pub magic: [u8; 4],
    reserved: [u8; 2],
    pub block_count: u16,
    pub uuid: [u8; 16],
    pub md5sum: [u8; 16],
    #[serde(with = "BigArray")]
    pub blockinfo: [VmaBlockInfo; VMA_CLUSTERS_PER_EXTENT],
}

#[derive(Clone, Eq, Hash, PartialEq)]
pub struct VmaConfig {
    pub name: String,
    pub content: String,
}

pub struct VmaReader<T> {
    vma_file: T,
    vma_header: VmaHeader,
    configs: HashSet<VmaConfig>,
}

impl<T: Read> VmaReader<T> {
    pub fn new(mut vma_file: T) -> Result<Self, Error> {
        let vma_header = Self::read_header(&mut vma_file)?;
        let configs = Self::read_configs(&vma_header)?;

        let instance = Self {
            vma_file,
            vma_header,
            configs,
        };

        Ok(instance)
    }

    fn read_header(vma_file: &mut T) -> Result<VmaHeader, Error> {
        let mut buffer = vec![0; VMA_HEADER_SIZE_NO_BLOB_BUFFER];
        vma_file.read_exact(&mut buffer).map_err(|e| {
            if e.kind() == io::ErrorKind::UnexpectedEof {
                io::Error::new(io::ErrorKind::UnexpectedEof, "Unexpected end of VMA file")
            } else {
                e
            }
        })?;

        let bincode_options = bincode::DefaultOptions::new()
            .with_fixint_encoding()
            .with_big_endian();

        let mut vma_header: VmaHeader = bincode_options.deserialize(&buffer)?;

        if vma_header.magic != VMA_HEADER_MAGIC {
            bail!("Invalid magic number");
        }

        if vma_header.version != 1 {
            bail!("Invalid VMA version {}", vma_header.version);
        }

        buffer.resize(vma_header.header_size as usize, 0);
        vma_file.read_exact(&mut buffer[VMA_HEADER_SIZE_NO_BLOB_BUFFER..])?;

        // Fill the MD5 sum field with zeros to compute the MD5 sum
        buffer[32..48].fill(0);
        let computed_md5sum: [u8; 16] = md5::compute(&buffer).into();

        if vma_header.md5sum != computed_md5sum {
            bail!("Wrong VMA header checksum");
        }

        let blob_buffer = &buffer[VMA_HEADER_SIZE_NO_BLOB_BUFFER..vma_header.header_size as usize];
        vma_header.blob_buffer = blob_buffer.to_vec();

        Ok(vma_header)
    }

    fn read_string(buffer: &[u8]) -> Result<String, Error> {
        let size_bytes: [u8; 2] = buffer[0..2].try_into()?;
        let size = u16::from_le_bytes(size_bytes) as usize;
        let string_bytes: &[u8] = &buffer[2..1 + size];
        let string = str::from_utf8(string_bytes)?;

        Ok(String::from(string))
    }

    fn read_configs(vma_header: &VmaHeader) -> Result<HashSet<VmaConfig>, Error> {
        let mut configs = HashSet::new();

        for i in 0..VMA_MAX_CONFIGS {
            let config_name_offset = vma_header.config_names[i] as usize;
            let config_data_offset = vma_header.config_data[i] as usize;

            if config_name_offset == 0 || config_data_offset == 0 {
                continue;
            }

            let config_name = Self::read_string(&vma_header.blob_buffer[config_name_offset..])?;
            let config_data = Self::read_string(&vma_header.blob_buffer[config_data_offset..])?;
            let vma_config = VmaConfig {
                name: config_name,
                content: config_data,
            };
            configs.insert(vma_config);
        }

        Ok(configs)
    }

    pub fn get_configs(&self) -> HashSet<VmaConfig> {
        self.configs.clone()
    }

    pub fn contains_device(&self, device_id: VmaDeviceId) -> bool {
        self.vma_header.dev_info[device_id as usize].device_name_offset != 0
    }

    pub fn get_device_name(&self, device_id: VmaDeviceId) -> Result<String, Error> {
        let device_name_offset =
            self.vma_header.dev_info[device_id as usize].device_name_offset as usize;

        if device_name_offset == 0 {
            bail!("device_name_offset cannot be 0");
        }

        let device_name = Self::read_string(&self.vma_header.blob_buffer[device_name_offset..])?;

        Ok(device_name)
    }

    pub fn get_device_size(&self, device_id: VmaDeviceId) -> Result<VmaDeviceSize, Error> {
        let dev_info = &self.vma_header.dev_info[device_id as usize];

        if dev_info.device_name_offset == 0 {
            bail!("device_name_offset cannot be 0");
        }

        Ok(dev_info.device_size)
    }

    fn read_extent_header(mut vma_file: impl Read) -> Result<VmaExtentHeader, Error> {
        let mut buffer = vec![0; size_of::<VmaExtentHeader>()];
        vma_file.read_exact(&mut buffer)?;

        let bincode_options = bincode::DefaultOptions::new()
            .with_fixint_encoding()
            .with_big_endian();

        let vma_extent_header: VmaExtentHeader = bincode_options.deserialize(&buffer)?;

        if vma_extent_header.magic != VMA_EXTENT_HEADER_MAGIC {
            bail!("Invalid magic number");
        }

        // Fill the MD5 sum field with zeros to compute the MD5 sum
        buffer[24..40].fill(0);
        let computed_md5sum: [u8; 16] = md5::compute(&buffer).into();

        if vma_extent_header.md5sum != computed_md5sum {
            bail!("Wrong VMA extent header checksum");
        }

        Ok(vma_extent_header)
    }

    fn restore_extent<F>(&mut self, callback: F) -> Result<(), Error>
    where
        F: Fn(VmaDeviceId, VmaDeviceOffset, Option<Vec<u8>>) -> Result<(), Error>,
    {
        let vma_extent_header = Self::read_extent_header(&mut self.vma_file)?;

        for cluster_index in 0..VMA_CLUSTERS_PER_EXTENT {
            let blockinfo = &vma_extent_header.blockinfo[cluster_index];

            if blockinfo.dev_id == 0 {
                continue;
            }

            let image_offset =
                (BLOCK_SIZE * VMA_BLOCKS_PER_CLUSTER * blockinfo.cluster_num as usize) as u64;
            let cluster_is_zero = blockinfo.mask == 0;

            let image_chunk_buffer = if cluster_is_zero {
                None
            } else {
                let mut image_chunk_buffer = vec![0; BLOCK_SIZE * VMA_BLOCKS_PER_CLUSTER];

                for block_index in 0..VMA_BLOCKS_PER_CLUSTER {
                    let block_is_zero = ((blockinfo.mask >> block_index) & 1) == 0;
                    let block_start = BLOCK_SIZE * block_index;
                    let block_end = block_start + BLOCK_SIZE;

                    if block_is_zero {
                        image_chunk_buffer[block_start..block_end].fill(0);
                    } else {
                        self.vma_file
                            .read_exact(&mut image_chunk_buffer[block_start..block_end])?;
                    }
                }

                Some(image_chunk_buffer)
            };

            callback(blockinfo.dev_id, image_offset, image_chunk_buffer)?;
        }

        Ok(())
    }

    pub fn restore<F>(&mut self, callback: F) -> Result<(), Error>
    where
        F: Fn(VmaDeviceId, VmaDeviceOffset, Option<Vec<u8>>) -> Result<(), Error>,
    {
        loop {
            match self.restore_extent(&callback) {
                Ok(()) => {}
                Err(e) => match e.downcast_ref::<io::Error>() {
                    Some(ioerr) => {
                        if ioerr.kind() == io::ErrorKind::UnexpectedEof {
                            break; // Break out of the loop since the end of the file was reached.
                        } else {
                            return Err(format_err!(e)).context("Failed to read VMA file");
                        }
                    }
                    _ => bail!(e),
                },
            }
        }

        Ok(())
    }
}
