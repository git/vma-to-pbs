use std::collections::HashMap;
use std::env::VarError::{NotPresent, NotUnicode};
use std::ffi::OsString;
use std::fs::read_dir;
use std::io::{BufRead, BufReader, Write};
use std::path::PathBuf;

use anyhow::{bail, Context, Error};
use chrono::NaiveDateTime;
use env_logger::Target;
use pbs_client::tools::get_secret_from_env;
use proxmox_sys::linux::tty;
use proxmox_time::epoch_i64;
use regex::Regex;

mod vma;
mod vma2pbs;
use vma2pbs::{vma2pbs, BackupVmaToPbsArgs, Compression, PbsArgs, VmaBackupArgs};

const CMD_HELP: &str = "\
Single VMA file usage:
vma-to-pbs [OPTIONS] --repository <auth_id@host:port:datastore> --vmid <VMID> [vma_file]

Bulk import usage:
vma-to-pbs [OPTIONS] --repository <auth_id@host:port:datastore> [--vmid <VMID>] [dump_directory]

Arguments:
  [vma_file | dump_directory]

Options:
      --repository <auth_id@host:port:datastore>
          Repository URL [env: PBS_REPOSITORY]
      [--ns <NAMESPACE>]
          Namespace
      [--vmid <VMID>]
          Backup ID
          This is required if a single VMA file is provided.
          If not specified, bulk import all VMA backups in the provided directory.
          If specified with a dump directory, only import backups of the specified vmid.
      [--backup-time <EPOCH>]
          Backup timestamp
      --fingerprint <FINGERPRINT>
          Proxmox Backup Server Fingerprint [env: PBS_FINGERPRINT]
      --keyfile <KEYFILE>
          Key file
      --master-keyfile <MASTER_KEYFILE>
          Master key file
  -c, --compress
          Compress the Backup
  -e, --encrypt
          Encrypt the Backup
      --password-file <PASSWORD_FILE>
          Password file [env: PBS_PASSWORD, PBS_PASSWORD_FD, PBS_PASSWORD_FILE, PBS_PASSWORD_CMD]
      --key-password-file <KEY_PASSWORD_FILE>
          Key password file [env: PBS_ENCRYPTION_PASSWORD, PBS_ENCRYPTION_PASSWORD_FD,
                             PBS_ENCRYPTION_PASSWORD_FILE, PBS_ENCRYPTION_PASSWORD_CMD]
      [--notes-file <NOTES_FILE>]
          File containing a comment/notes
      [--log-file <LOG_FILE>]
          Log file
      --skip-failed
          Skip VMIDs that failed to be uploaded and continue onto the next VMID if a dump directory
          is specified.
  -y, --yes
          Automatic yes to prompts
  -h, --help
          Print help
  -V, --version
          Print version
";
const VERSION: &str = env!("CARGO_PKG_VERSION");

fn parse_args() -> Result<BackupVmaToPbsArgs, Error> {
    let mut args: Vec<_> = std::env::args_os().collect();
    args.remove(0); // remove the executable path.

    let mut first_later_args_index = 0;
    let options = [
        "-h",
        "--help",
        "-V",
        "--version",
        "-c",
        "--compress",
        "-e",
        "--encrypt",
        "--skip-failed",
        "-y",
        "--yes",
    ];

    for (i, arg) in args.iter().enumerate() {
        if let Some(arg) = arg.to_str() {
            if arg.starts_with('-') {
                if arg == "--" {
                    args.remove(i);
                    first_later_args_index = i;
                    break;
                }

                first_later_args_index = i + 1;

                if !options.contains(&arg) {
                    first_later_args_index += 1;
                }
            }
        }
    }

    let forwarded_args = if first_later_args_index > args.len() {
        Vec::new()
    } else {
        args.split_off(first_later_args_index)
    };

    let mut args = pico_args::Arguments::from_vec(args);

    if args.contains(["-h", "--help"]) {
        print!("{CMD_HELP}");
        std::process::exit(0);
    } else if args.contains(["-V", "--version"]) {
        println!("vma-to-pbs version {VERSION}");
        std::process::exit(0);
    }

    let pbs_repository = args.opt_value_from_str("--repository")?;
    let namespace = args.opt_value_from_str("--ns")?;
    let vmid: Option<String> = args.opt_value_from_str("--vmid")?;
    let backup_time: Option<i64> = args.opt_value_from_str("--backup-time")?;
    let backup_time = backup_time.unwrap_or_else(epoch_i64);
    let fingerprint = args.opt_value_from_str("--fingerprint")?;
    let keyfile = args.opt_value_from_str("--keyfile")?;
    let master_keyfile = args.opt_value_from_str("--master-keyfile")?;
    let compress = args.contains(["-c", "--compress"]);
    let encrypt = args.contains(["-e", "--encrypt"]);
    let password_file: Option<OsString> = args.opt_value_from_str("--password-file")?;
    let key_password_file: Option<OsString> = args.opt_value_from_str("--key-password-file")?;
    let notes_file: Option<OsString> = args.opt_value_from_str("--notes-file")?;
    let log_file_path: Option<OsString> = args.opt_value_from_str("--log-file")?;
    let skip_failed = args.contains("--skip-failed");
    let yes = args.contains(["-y", "--yes"]);

    match (encrypt, keyfile.is_some()) {
        (true, false) => bail!("--encrypt requires a --keyfile!"),
        (false, true) => log::info!(
            "--keyfile given, but --encrypt not set -> backup will be signed, but not encrypted!"
        ),
        _ => {}
    }

    if !args.finish().is_empty() {
        bail!("unexpected extra arguments, use '-h' for usage");
    }

    let pbs_repository = match pbs_repository {
        Some(v) => v,
        None => match std::env::var("PBS_REPOSITORY") {
            Ok(v) => v,
            Err(NotPresent) => bail!("Repository not set. Use $PBS_REPOSITORY or --repository"),
            Err(NotUnicode(_)) => bail!("$PBS_REPOSITORY contains invalid unicode"),
        },
    };

    let fingerprint = match fingerprint {
        Some(v) => v,
        None => match std::env::var("PBS_FINGERPRINT") {
            Ok(v) => v,
            Err(NotPresent) => bail!("Fingerprint not set. Use $PBS_FINGERPRINT or --fingerprint"),
            Err(NotUnicode(_)) => bail!("$PBS_FINGERPRINT contains invalid unicode"),
        },
    };

    if forwarded_args.len() > 1 {
        bail!("too many arguments");
    }

    let vma_file_path = forwarded_args.first();

    let pbs_password = if let Some(password_file) = password_file {
        let mut password =
            std::fs::read_to_string(password_file).context("Could not read password file")?;

        if password.ends_with('\n') || password.ends_with('\r') {
            password.pop();
            if password.ends_with('\r') {
                password.pop();
            }
        }

        password
    } else if let Some(password) = get_secret_from_env("PBS_PASSWORD")? {
        password
    } else if vma_file_path.is_none() {
        bail!(
            "Please use --password-file, $PBS_PASSWORD, $PBS_PASSWORD_FD, $PBS_PASSWORD_FILE, \
            or $PBS_PASSWORD_CMD to provide the password when passing the VMA file to stdin"
        );
    } else {
        String::from_utf8(tty::read_password("Password: ")?)?
    };

    let key_password = if keyfile.is_some() {
        if let Some(key_password_file) = key_password_file {
            let mut key_password = std::fs::read_to_string(key_password_file)
                .context("Could not read key password file")?;

            if key_password.ends_with('\n') || key_password.ends_with('\r') {
                key_password.pop();
                if key_password.ends_with('\r') {
                    key_password.pop();
                }
            }

            Some(key_password)
        } else if let Some(key_password) = get_secret_from_env("PBS_ENCRYPTION_PASSWORD")? {
            Some(key_password)
        } else if vma_file_path.is_none() {
            log::info!(
                "Please use --key-password-file to provide the password when passing the VMA file \
                to stdin, if required."
            );
            None
        } else {
            Some(String::from_utf8(tty::read_password("Key Password: ")?)?)
        }
    } else {
        None
    };

    let notes = if let Some(notes_file) = notes_file {
        let notes = std::fs::read_to_string(notes_file).context("Could not read notes file")?;

        Some(notes)
    } else {
        None
    };

    let pbs_args = PbsArgs {
        pbs_repository,
        namespace,
        pbs_password,
        keyfile,
        key_password,
        master_keyfile,
        fingerprint,
        compress,
        encrypt,
    };

    let bulk =
        vma_file_path
            .map(PathBuf::from)
            .and_then(|path| if path.is_dir() { Some(path) } else { None });

    let grouped_vmas = if let Some(dump_dir_path) = bulk {
        let re = Regex::new(
            r"vzdump-qemu-(\d+)-(\d{4}_\d{2}_\d{2}-\d{2}_\d{2}_\d{2}).vma(|.zst|.lzo|.gz)$",
        )?;

        let mut vmas = Vec::new();

        for entry in read_dir(dump_dir_path)? {
            let entry = entry?;
            let path = entry.path();

            if !path.is_file() {
                continue;
            }

            if let Some(file_name) = path.file_name().and_then(|n| n.to_str()) {
                let Some((_, [backup_id, timestr, ext])) =
                    re.captures(file_name).map(|c| c.extract())
                else {
                    log::debug!("Skip \"{file_name}\", since it is not a VMA backup");
                    continue;
                };

                if let Some(ref vmid) = vmid {
                    if backup_id != vmid {
                        log::debug!(
                            "Skip backup with VMID {}, since it does not match specified VMID {}",
                            backup_id,
                            vmid
                        );
                        continue;
                    }
                }

                let compression = match ext {
                    "" => None,
                    ".zst" => Some(Compression::Zstd),
                    ".lzo" => Some(Compression::Lzo),
                    ".gz" => Some(Compression::GZip),
                    _ => bail!("Unexpected file extension: {ext}"),
                };

                let backup_time = NaiveDateTime::parse_from_str(timestr, "%Y_%m_%d-%H_%M_%S")?
                    .and_utc()
                    .timestamp();

                let notes_path = path.with_file_name(format!("{file_name}.notes"));
                let notes = proxmox_sys::fs::file_read_optional_string(notes_path)?;

                let log_path = path.with_file_name(format!("{file_name}.log"));
                let log_file_path = if log_path.exists() {
                    Some(log_path.to_path_buf().into_os_string())
                } else {
                    None
                };

                let backup_args = VmaBackupArgs {
                    vma_file_path: Some(path.clone().into()),
                    compression,
                    backup_id: backup_id.to_string(),
                    backup_time,
                    notes,
                    log_file_path,
                };
                vmas.push(backup_args);
            }
        }

        vmas.sort_by_key(|d| d.backup_time);
        let total_vma_count = vmas.len();
        let grouped_vmas = vmas.into_iter().fold(
            HashMap::new(),
            |mut grouped: HashMap<String, Vec<VmaBackupArgs>>, vma_args| {
                grouped
                    .entry(vma_args.backup_id.clone())
                    .or_default()
                    .push(vma_args);
                grouped
            },
        );

        if grouped_vmas.is_empty() {
            bail!("Did not find any backup archives");
        }

        log::info!(
            "Found {total_vma_count} backup archive(s) of {} different VMID(s):",
            grouped_vmas.len()
        );

        for (backup_id, vma_group) in &grouped_vmas {
            log::info!("- VMID {backup_id}: {} backups", vma_group.len());
        }

        if !yes {
            eprint!("Proceed with the bulk import? (Y/n): ");
            std::io::stdout().flush()?;
            let mut line = String::new();

            BufReader::new(std::io::stdin()).read_line(&mut line)?;
            let trimmed = line.trim();
            match trimmed {
                "y" | "Y" | "" => {}
                "n" | "N" => bail!("Bulk import was not confirmed."),
                _ => bail!("Unexpected choice '{trimmed}'!"),
            }
        }

        grouped_vmas
    } else if let Some(vmid) = vmid {
        HashMap::from([(
            vmid.clone(),
            vec![VmaBackupArgs {
                vma_file_path: vma_file_path.cloned(),
                compression: None,
                backup_id: vmid,
                backup_time,
                notes,
                log_file_path,
            }],
        )])
    } else {
        bail!("No vmid specified for single backup file");
    };

    let options = BackupVmaToPbsArgs {
        pbs_args,
        grouped_vmas,
        skip_failed,
    };

    Ok(options)
}

fn init_cli_logger() {
    env_logger::Builder::from_env(env_logger::Env::new().filter_or("RUST_LOG", "info"))
        .format_level(false)
        .format_target(false)
        .format_timestamp(None)
        .target(Target::Stdout)
        .init();
}

fn main() -> Result<(), Error> {
    init_cli_logger();

    let args = parse_args()?;
    vma2pbs(args)?;

    Ok(())
}
