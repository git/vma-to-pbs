use std::cell::RefCell;
use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::ffi::{c_char, CStr, CString, OsString};
use std::fs::File;
use std::io::{stdin, BufRead, BufReader, Read};
use std::process::{Child, Command, Stdio};
use std::ptr;
use std::sync::atomic::{AtomicU64, Ordering};
use std::sync::Arc;
use std::time::SystemTime;

use anyhow::{anyhow, bail, Error};
use pbs_api_types::{BackupDir, BackupNamespace, BackupType};
use pbs_client::{BackupRepository, HttpClient, HttpClientOptions};
use pbs_datastore::DataBlob;
use pbs_key_config::decrypt_key;
use pbs_tools::crypt_config::CryptConfig;
use proxmox_async::runtime::block_on;
use proxmox_backup_qemu::{
    capi_types::ProxmoxBackupHandle, proxmox_backup_add_config, proxmox_backup_close_image,
    proxmox_backup_connect, proxmox_backup_disconnect, proxmox_backup_finish,
    proxmox_backup_new_ns, proxmox_backup_register_image, proxmox_backup_write_data,
    PROXMOX_BACKUP_DEFAULT_CHUNK_SIZE,
};
use proxmox_time::epoch_to_rfc3339;
use scopeguard::defer;
use serde_json::Value;

use crate::vma::{VmaReader, VMA_MAX_DEVICES};

const VMA_CLUSTER_SIZE: usize = 65536;

pub struct BackupVmaToPbsArgs {
    pub pbs_args: PbsArgs,
    pub grouped_vmas: HashMap<String, Vec<VmaBackupArgs>>,
    pub skip_failed: bool,
}

pub struct PbsArgs {
    pub pbs_repository: String,
    pub namespace: Option<String>,
    pub pbs_password: String,
    pub keyfile: Option<String>,
    pub key_password: Option<String>,
    pub master_keyfile: Option<String>,
    pub fingerprint: String,
    pub compress: bool,
    pub encrypt: bool,
}

pub enum Compression {
    Zstd,
    Lzo,
    GZip,
}

pub struct VmaBackupArgs {
    pub vma_file_path: Option<OsString>,
    pub compression: Option<Compression>,
    pub backup_id: String,
    pub backup_time: i64,
    pub notes: Option<String>,
    pub log_file_path: Option<OsString>,
}

#[derive(Copy, Clone)]
struct BlockDeviceInfo {
    pub pbs_device_id: u8,
    pub device_size: u64,
}

fn handle_pbs_error(pbs_err: *mut c_char, function_name: &str) -> Result<(), Error> {
    if pbs_err.is_null() {
        bail!("{function_name} failed without error message");
    }

    let pbs_err_cstr = unsafe { CStr::from_ptr(pbs_err) };
    let pbs_err_str = pbs_err_cstr.to_string_lossy();
    bail!("{function_name} failed: {pbs_err_str}");
}

fn create_pbs_backup_task(
    pbs_args: &PbsArgs,
    backup_args: &VmaBackupArgs,
) -> Result<*mut ProxmoxBackupHandle, Error> {
    log::info!(
        "\tbackup time: {}",
        epoch_to_rfc3339(backup_args.backup_time)?
    );

    let mut pbs_err: *mut c_char = ptr::null_mut();

    let pbs_repository_cstr = CString::new(pbs_args.pbs_repository.as_str())?;
    let ns_cstr = CString::new(pbs_args.namespace.as_deref().unwrap_or(""))?;
    let backup_id_cstr = CString::new(backup_args.backup_id.as_str())?;
    let pbs_password_cstr = CString::new(pbs_args.pbs_password.as_str())?;
    let fingerprint_cstr = CString::new(pbs_args.fingerprint.as_str())?;
    let keyfile_cstr = pbs_args
        .keyfile
        .as_ref()
        .map(|v| CString::new(v.as_str()).unwrap());
    let keyfile_ptr = keyfile_cstr
        .as_ref()
        .map(|v| v.as_ptr())
        .unwrap_or(ptr::null());
    let key_password_cstr = pbs_args
        .key_password
        .as_ref()
        .map(|v| CString::new(v.as_str()).unwrap());
    let key_password_ptr = key_password_cstr
        .as_ref()
        .map(|v| v.as_ptr())
        .unwrap_or(ptr::null());
    let master_keyfile_cstr = pbs_args
        .master_keyfile
        .as_ref()
        .map(|v| CString::new(v.as_str()).unwrap());
    let master_keyfile_ptr = master_keyfile_cstr
        .as_ref()
        .map(|v| v.as_ptr())
        .unwrap_or(ptr::null());

    let pbs = proxmox_backup_new_ns(
        pbs_repository_cstr.as_ptr(),
        ns_cstr.as_ptr(),
        backup_id_cstr.as_ptr(),
        backup_args.backup_time as u64,
        PROXMOX_BACKUP_DEFAULT_CHUNK_SIZE,
        pbs_password_cstr.as_ptr(),
        keyfile_ptr,
        key_password_ptr,
        master_keyfile_ptr,
        pbs_args.compress,
        pbs_args.encrypt,
        fingerprint_cstr.as_ptr(),
        &mut pbs_err,
    );

    if pbs.is_null() {
        handle_pbs_error(pbs_err, "proxmox_backup_new_ns")?;
    }

    Ok(pbs)
}

fn upload_configs<T>(vma_reader: &VmaReader<T>, pbs: *mut ProxmoxBackupHandle) -> Result<(), Error>
where
    T: Read,
{
    let mut pbs_err: *mut c_char = ptr::null_mut();
    let configs = vma_reader.get_configs();
    for config in configs {
        let config_name = config.name;
        let config_data = config.content;

        log::info!("\tCFG: size: {} name: {config_name}", config_data.len());

        let config_name_cstr = CString::new(config_name)?;

        if proxmox_backup_add_config(
            pbs,
            config_name_cstr.as_ptr(),
            config_data.as_ptr(),
            config_data.len() as u64,
            &mut pbs_err,
        ) < 0
        {
            handle_pbs_error(pbs_err, "proxmox_backup_add_config")?;
        }
    }

    Ok(())
}

fn register_block_devices<T>(
    vma_reader: &VmaReader<T>,
    pbs: *mut ProxmoxBackupHandle,
) -> Result<[Option<BlockDeviceInfo>; VMA_MAX_DEVICES], Error>
where
    T: Read,
{
    let mut block_device_infos: [Option<BlockDeviceInfo>; VMA_MAX_DEVICES] =
        [None; VMA_MAX_DEVICES];
    let mut pbs_err: *mut c_char = ptr::null_mut();

    for (device_id, block_device_info) in block_device_infos.iter_mut().enumerate() {
        if !vma_reader.contains_device(device_id.try_into()?) {
            continue;
        }

        let device_name = vma_reader.get_device_name(device_id.try_into()?)?;
        let device_size = vma_reader.get_device_size(device_id.try_into()?)?;

        log::info!("\tDEV: dev_id={device_id} size: {device_size} devname: {device_name}");

        let device_name_cstr = CString::new(device_name)?;
        let pbs_device_id = proxmox_backup_register_image(
            pbs,
            device_name_cstr.as_ptr(),
            device_size,
            false,
            &mut pbs_err,
        );

        if pbs_device_id < 0 {
            handle_pbs_error(pbs_err, "proxmox_backup_register_image")?;
        }

        *block_device_info = Some(BlockDeviceInfo {
            pbs_device_id: pbs_device_id as u8,
            device_size,
        });
    }

    Ok(block_device_infos)
}

fn upload_block_devices<T>(
    mut vma_reader: VmaReader<T>,
    pbs: *mut ProxmoxBackupHandle,
) -> Result<(), Error>
where
    T: Read,
{
    let block_device_infos = register_block_devices(&vma_reader, pbs)?;

    struct ImageChunk {
        sub_chunks: HashMap<u8, Option<Vec<u8>>>,
        mask: u64,
        non_zero_mask: u64,
    }

    let chunk_stats = Arc::new([const { AtomicU64::new(0) }; VMA_MAX_DEVICES]);

    let images_chunks: RefCell<HashMap<u8, HashMap<u64, ImageChunk>>> =
        RefCell::new(HashMap::new());

    vma_reader.restore(|dev_id, offset, buffer| {
        let block_device_info = match block_device_infos[dev_id as usize] {
            Some(block_device_info) => block_device_info,
            None => bail!("Reference to unknown device id {} in VMA file", dev_id),
        };

        let pbs_device_id = block_device_info.pbs_device_id;
        let device_size = block_device_info.device_size;

        let mut images_chunks = images_chunks.borrow_mut();
        let image_chunks = match images_chunks.entry(dev_id) {
            Entry::Occupied(image_chunks) => image_chunks.into_mut(),
            Entry::Vacant(image_chunks) => image_chunks.insert(HashMap::new()),
        };
        let pbs_chunk_offset =
            PROXMOX_BACKUP_DEFAULT_CHUNK_SIZE * (offset / PROXMOX_BACKUP_DEFAULT_CHUNK_SIZE);
        let sub_chunk_index =
            ((offset % PROXMOX_BACKUP_DEFAULT_CHUNK_SIZE) / VMA_CLUSTER_SIZE as u64) as u8;

        let pbs_chunk_size = PROXMOX_BACKUP_DEFAULT_CHUNK_SIZE.min(device_size - pbs_chunk_offset);

        let prepare_pbs_chunk = |sub_chunk_count: u8, image_chunk: &ImageChunk| {
            let mut pbs_chunk_buffer = proxmox_io::boxed::zeroed(pbs_chunk_size as usize);

            for i in 0..sub_chunk_count {
                let sub_chunk = &image_chunk.sub_chunks[&i];
                let start = i as usize * VMA_CLUSTER_SIZE;
                let end = (start + VMA_CLUSTER_SIZE).min(pbs_chunk_size as usize);

                match sub_chunk {
                    Some(sub_chunk) => {
                        pbs_chunk_buffer[start..end].copy_from_slice(&sub_chunk[0..end - start]);
                    }
                    None => pbs_chunk_buffer[start..end].fill(0),
                }
            }

            pbs_chunk_buffer
        };

        let pbs_upload_chunk = |pbs_chunk_buffer: Option<&[u8]>| {
            log::debug!(
                "\tUploading dev_id: {dev_id} offset: {pbs_chunk_offset:#0X} - {:#0X}",
                pbs_chunk_offset + pbs_chunk_size,
            );
            let chunk_stat = chunk_stats[dev_id as usize].fetch_add(1, Ordering::SeqCst);
            if (chunk_stat % 1000) == 0 {
                let percentage = 100 * PROXMOX_BACKUP_DEFAULT_CHUNK_SIZE * chunk_stat / device_size;
                log::info!("\tUploading dev_id: {dev_id} ({percentage}%)");
            }

            let mut pbs_err: *mut c_char = ptr::null_mut();

            let write_data_result = proxmox_backup_write_data(
                pbs,
                pbs_device_id,
                match pbs_chunk_buffer {
                    Some(pbs_chunk_buffer) => pbs_chunk_buffer.as_ptr(),
                    None => ptr::null(),
                },
                pbs_chunk_offset,
                pbs_chunk_size,
                &mut pbs_err,
            );

            if write_data_result < 0 {
                handle_pbs_error(pbs_err, "proxmox_backup_write_data")?;
            }

            Ok::<(), Error>(())
        };

        let insert_image_chunk = |image_chunks: &mut HashMap<u64, ImageChunk>,
                                  buffer: Option<Vec<u8>>| {
            let mut sub_chunks: HashMap<u8, Option<Vec<u8>>> = HashMap::new();
            let mask = 1 << sub_chunk_index;
            let non_zero_mask = buffer.is_some() as u64;
            sub_chunks.insert(sub_chunk_index, buffer);

            let image_chunk = ImageChunk {
                sub_chunks,
                mask,
                non_zero_mask,
            };

            image_chunks.insert(pbs_chunk_offset, image_chunk);
        };

        let image_chunk = image_chunks.get_mut(&pbs_chunk_offset);

        match image_chunk {
            Some(image_chunk) => {
                image_chunk.mask |= 1 << sub_chunk_index;
                image_chunk.non_zero_mask |= (buffer.is_some() as u64) << sub_chunk_index;
                image_chunk.sub_chunks.insert(sub_chunk_index, buffer);

                let sub_chunk_count = ((pbs_chunk_size + 65535) / VMA_CLUSTER_SIZE as u64) as u8;
                let pbs_chunk_mask = 1_u64
                    .checked_shl(sub_chunk_count.into())
                    .unwrap_or(0)
                    .wrapping_sub(1);

                if image_chunk.mask == pbs_chunk_mask {
                    if image_chunk.non_zero_mask == 0 {
                        pbs_upload_chunk(None)?;
                    } else {
                        let pbs_chunk_buffer = prepare_pbs_chunk(sub_chunk_count, image_chunk);
                        pbs_upload_chunk(Some(&*pbs_chunk_buffer))?;
                    }

                    image_chunks.remove(&pbs_chunk_offset);
                }
            }
            None => {
                if pbs_chunk_size <= VMA_CLUSTER_SIZE as u64 {
                    pbs_upload_chunk(buffer.as_deref())?;
                } else {
                    insert_image_chunk(image_chunks, buffer);
                }
            }
        }

        Ok(())
    })?;

    let mut pbs_err: *mut c_char = ptr::null_mut();

    for block_device_info in block_device_infos {
        let block_device_info = match block_device_info {
            Some(block_device_info) => block_device_info,
            None => continue,
        };

        let pbs_device_id = block_device_info.pbs_device_id;

        if proxmox_backup_close_image(pbs, pbs_device_id, &mut pbs_err) < 0 {
            handle_pbs_error(pbs_err, "proxmox_backup_close_image")?;
        }
    }

    Ok(())
}

fn pbs_client_setup(
    pbs_args: &PbsArgs,
    backup_args: &VmaBackupArgs,
) -> Result<(HttpClient, String, Value), Error> {
    let repo: BackupRepository = pbs_args.pbs_repository.parse()?;
    let options = HttpClientOptions::new_interactive(
        Some(pbs_args.pbs_password.clone()),
        Some(pbs_args.fingerprint.clone()),
    );
    let client = HttpClient::new(repo.host(), repo.port(), repo.auth_id(), options)?;

    let backup_dir = BackupDir::from((
        BackupType::Vm,
        backup_args.backup_id.clone(),
        backup_args.backup_time,
    ));

    let namespace = match &pbs_args.namespace {
        Some(namespace) => BackupNamespace::new(namespace)?,
        None => BackupNamespace::root(),
    };

    let mut request_args = serde_json::to_value(backup_dir)?;
    if !namespace.is_root() {
        request_args["ns"] = serde_json::to_value(namespace)?;
    }

    Ok((client, repo.store().to_owned(), request_args))
}

fn upload_log(
    client: &HttpClient,
    log_file_path: &OsString,
    pbs_args: &PbsArgs,
    store: &str,
    request_args: Value,
) -> Result<(), Error> {
    let path = format!("api2/json/admin/datastore/{}/upload-backup-log", store);
    let data = std::fs::read(log_file_path)?;

    let blob = if pbs_args.encrypt {
        let crypt_config = match &pbs_args.keyfile {
            None => None,
            Some(keyfile) => {
                let key = std::fs::read(keyfile)?;
                let (key, _created, _) = decrypt_key(&key, &|| -> Result<Vec<u8>, Error> {
                    match &pbs_args.key_password {
                        Some(key_password) => Ok(key_password.clone().into_bytes()),
                        None => bail!("no key password provided"),
                    }
                })?;
                let crypt_config = CryptConfig::new(key)?;
                Some(crypt_config)
            }
        };

        DataBlob::encode(&data, crypt_config.as_ref(), pbs_args.compress)?
    } else {
        // fixme: howto sign log?
        DataBlob::encode(&data, None, pbs_args.compress)?
    };

    let body = hyper::Body::from(blob.into_inner());

    block_on(async {
        client
            .upload("application/octet-stream", body, &path, Some(request_args))
            .await
            .unwrap();
    });

    Ok(())
}

fn set_notes(
    client: &HttpClient,
    notes: &str,
    store: &str,
    mut request_args: Value,
) -> Result<(), Error> {
    request_args["notes"] = Value::from(notes);
    let path = format!("api2/json/admin/datastore/{}/notes", store);
    block_on(async {
        client.put(&path, Some(request_args)).await.unwrap();
    });

    Ok(())
}

pub fn vma2pbs(args: BackupVmaToPbsArgs) -> Result<(), Error> {
    let pbs_args = &args.pbs_args;
    log::info!("PBS repository: {}", pbs_args.pbs_repository);
    if let Some(ns) = &pbs_args.namespace {
        log::info!("PBS namespace: {ns}");
    }
    log::info!("PBS fingerprint: {}", pbs_args.fingerprint);
    log::info!("compress: {}", pbs_args.compress);
    log::info!("encrypt: {}", pbs_args.encrypt);

    let start_transfer_time = SystemTime::now();

    for (_, vma_group) in args.grouped_vmas {
        for backup_args in vma_group {
            if let Err(e) = upload_vma_file(pbs_args, &backup_args) {
                let err_msg = format!(
                    "Failed to upload vma file at {:?} - {e}",
                    backup_args.vma_file_path.unwrap_or("(stdin)".into()),
                );

                if args.skip_failed {
                    log::warn!("{}", err_msg);
                    log::info!("Skipping VMID {}", backup_args.backup_id);
                    break;
                } else {
                    bail!(err_msg);
                }
            }
        }
    }

    let transfer_duration = SystemTime::now().duration_since(start_transfer_time)?;
    let total_seconds = transfer_duration.as_secs();
    let minutes = total_seconds / 60;
    let seconds = total_seconds % 60;
    let milliseconds = transfer_duration.as_millis() % 1000;
    log::info!("Backup finished within {minutes} minutes, {seconds} seconds and {milliseconds} ms");

    Ok(())
}

fn upload_vma_file(pbs_args: &PbsArgs, backup_args: &VmaBackupArgs) -> Result<(), Error> {
    match &backup_args.vma_file_path {
        Some(vma_file_path) => log::info!("Uploading VMA backup from {vma_file_path:?}"),
        None => log::info!("Uploading VMA backup from (stdin)"),
    };

    let (vma_file, decompproc): (Box<dyn BufRead>, Option<Child>) = match &backup_args.compression {
        Some(compression) => {
            let vma_file_path = backup_args
                .vma_file_path
                .as_ref()
                .expect("No VMA file path provided");
            let mut cmd = match compression {
                Compression::Zstd => {
                    let mut cmd = Command::new("zstd");
                    cmd.args(["-q", "-d", "-c"]);
                    cmd
                }
                Compression::Lzo => {
                    let mut cmd = Command::new("lzop");
                    cmd.args(["-d", "-c"]);
                    cmd
                }
                Compression::GZip => Command::new("zcat"),
            };
            let mut process = cmd.arg(vma_file_path).stdout(Stdio::piped()).spawn()?;
            let stdout = process.stdout.take().expect("Failed to capture stdout");
            (Box::new(BufReader::new(stdout)), Some(process))
        }
        None => match &backup_args.vma_file_path {
            Some(vma_file_path) => match File::open(vma_file_path) {
                Err(why) => return Err(anyhow!("Couldn't open file: {why}")),
                Ok(file) => (Box::new(BufReader::new(file)), None),
            },
            None => (Box::new(BufReader::new(stdin())), None),
        },
    };

    let vma_reader = VmaReader::new(vma_file)?;

    let pbs = create_pbs_backup_task(pbs_args, backup_args)?;

    defer! {
        proxmox_backup_disconnect(pbs);
    }

    let mut pbs_err: *mut c_char = ptr::null_mut();
    let connect_result = proxmox_backup_connect(pbs, &mut pbs_err);

    if connect_result < 0 {
        handle_pbs_error(pbs_err, "proxmox_backup_connect")?;
    }

    upload_configs(&vma_reader, pbs)?;
    upload_block_devices(vma_reader, pbs)?;

    if let Some(mut decompproc) = decompproc {
        let status = decompproc.wait()?;

        if !status.success() {
            bail!("Failed to decompress file with exit code: {status}");
        }
    }

    if proxmox_backup_finish(pbs, &mut pbs_err) < 0 {
        handle_pbs_error(pbs_err, "proxmox_backup_finish")?;
    }

    if backup_args.notes.is_some() || backup_args.log_file_path.is_some() {
        let (client, store, request_args) = pbs_client_setup(pbs_args, backup_args)?;

        if let Some(log_file_path) = &backup_args.log_file_path {
            upload_log(
                &client,
                log_file_path,
                pbs_args,
                &store,
                request_args.clone(),
            )?;
        }

        if let Some(notes) = &backup_args.notes {
            set_notes(&client, notes, &store, request_args)?;
        }
    }

    Ok(())
}
