include /usr/share/dpkg/default.mk

# So that pbs-buildcfg uses this instead of trying to call git rev-parse inside
# the vendored sources...
-include REPOID

PACKAGE = proxmox-vma-to-pbs
ARCH := $(DEB_BUILD_ARCH)
#
# build in separate directory but output resulting package artefacts top-level by default
# allow to override by passing OUTPUT_DIR explicitly, e.g.: make OUTPUT_DIR=build/ deb
OUTPUT_DIR :=
BUILD_DIR := $(OUTPUT_DIR)$(PACKAGE)-$(DEB_VERSION)

DEB=$(PACKAGE)_$(DEB_VERSION)_$(ARCH).deb
DEB_DBGSYM=$(PACKAGE)-dbgsym_$(DEB_VERSION)_$(ARCH).deb
DSC=$(PACKAGE)_$(DEB_VERSION).dsc

DESTDIR=

TARGET_DIR := target/debug

ifeq ($(BUILD_MODE), release)
CARGO_BUILD_ARGS += --release
TARGETDIR := target/release
endif

CARGO ?= cargo

USED_PBS_CRATES := \
	pbs-api-types \
	pbs-buildcfg \
	pbs-client \
	pbs-config \
	pbs-datastore \
	pbs-key-config \
	pbs-tools

.PHONY: all build
all: build

build: $(TARGETDIR)/vma-to-pbs
$(TARGETDIR)/vma-to-pbs: Cargo.toml src/
	$(CARGO) build $(CARGO_BUILD_ARGS)

.PHONY: install
install: $(TARGETDIR)/vma-to-pbs
	$(CARGO) install

.PHONY: build-dir
build-dir:
	rm -rf $(BUILD_DIR)
	$(MAKE) $(BUILD_DIR)

$(BUILD_DIR): submodule
	rm -rf $@ $@.tmp
	mkdir -p $@.tmp
	echo system >$@.tmp/rust-toolchain
	cp -t $@.tmp -a \
	  debian \
	  Makefile \
	  .cargo \
	  Cargo.toml \
	  src
	# ---8<--- "vendor" the submodule...
	mkdir -p $@.tmp/submodules/proxmox-backup-qemu
	cp -t $@.tmp/submodules/proxmox-backup-qemu -a \
	  submodules/proxmox-backup-qemu/Cargo.toml \
	  submodules/proxmox-backup-qemu/src \
	  submodules/proxmox-backup-qemu/build.rs \
	  submodules/proxmox-backup-qemu/current-api.h \
	  submodules/proxmox-backup-qemu/header-preamble.c
	mkdir -p $@.tmp/submodules/proxmox-backup-qemu/submodules/proxmox-backup
	cp -t $@.tmp/submodules/proxmox-backup-qemu/submodules/proxmox-backup -a \
	  $(addprefix submodules/proxmox-backup-qemu/submodules/proxmox-backup/,Cargo.toml $(USED_PBS_CRATES))
	( \
	  printf "export REPOID="; \
	  cd submodules/proxmox-backup-qemu/submodules/proxmox-backup && git rev-parse HEAD \
	) >$@.tmp/REPOID
	# --->8---
	rm -f $@.tmp/Cargo.lock
	mv $@.tmp $@

# Run `make gendeps >deps` and copy the command's output to d/control to update
# the control file...
gendeps:
	cargo-fairy d-control \
	  ./Cargo.toml \
	  submodules/proxmox-backup-qemu/Cargo.toml \
	  $(addprefix submodules/proxmox-backup-qemu/submodules/proxmox-backup/,$(addsuffix /Cargo.toml,$(USED_PBS_CRATES)))

submodule:
	[ -e submodules/proxmox-backup-qemu/submodules/proxmox-backup/Cargo.toml ] || git submodule update --init --recursive

.PHONY: deb
deb:
	rm -rf $(BUILD_DIR)
	$(MAKE) $(OUTPUT_DIR)$(DEB)

$(OUTPUT_DIR)$(DEB_DBGSYM): $(OUTPUT_DIR)$(DEB)
$(OUTPUT_DIR)$(DEB): $(BUILD_DIR)
	cd $(BUILD_DIR) && dpkg-buildpackage -b -uc -us
	lintian $@

.PHONY: dsc
dsc:
	rm -rf $(BUILD_DIR)
	$(MAKE) $(OUTPUT_DIR)$(DSC)
	lintian $(OUTPUT_DIR)$(DSC)

$(OUTPUT_DIR)$(DSC): $(BUILD_DIR)
	cd $(BUILD_DIR) && dpkg-buildpackage -S -uc -us -d

sbuild: $(OUTPUT_DIR)$(DSC)
	[ -z "$(OUTPUT_DIR)" ] || cd $(OUTPUT_DIR); sbuild $(DSC)

.PHONY: clean
clean:
	rm -rf $(BUILD_DIR) $(PACKAGE)-[0-9]*/
	[ -z "$(OUTPUT_DIR)" ] || rm -rf $(OUTPUT_DIR)
	rm -f *.deb *.dsc *.buildinfo *.build *.changes  $(PACKAGE)*.tar*
	$(CARGO) clean

.PHONY: upload
upload: UPLOAD_DIST ?= $(DEB_DISTRIBUTION)
upload: $(OUTPUT_DIR)$(DEB) $(OUTPUT_DIR)$(DEB_DBGSYM)
	[ -z "$(OUTPUT_DIR)" ] || cd $(OUTPUT_DIR); \
	  tar cf - $(DEB) $(DEB_DBGSYM) | ssh -X repoman@repo.proxmox.com upload --product pve,pbs --dist $(UPLOAD_DIST)
